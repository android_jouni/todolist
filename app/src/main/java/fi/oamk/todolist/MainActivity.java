package fi.oamk.todolist;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements TodoAdapter.TaskClickListener{

    private TodoAdapter adapter;
    private ArrayList<TaskItem> tasks = new ArrayList<>();
    private int position = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Set recyclerview.

        tasks.add(new TaskItem("Test 1",1));
        tasks.add(new TaskItem("Test 2",1));
        tasks.add(new TaskItem("Test 3",1));


        //task = myApi.getALl();

        RecyclerView recyclerView = findViewById(R.id.rvTasks);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new TodoAdapter(this,tasks);
        adapter.setClickListener(this);
        recyclerView.setAdapter(adapter);

        // Set toolbar.
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Set FloatingActionButton
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, Task.class );
                startActivityForResult(intent,Task.TASK_ADD);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to
        if (requestCode == Task.TASK_ADD) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {
                String task_name = data.getStringExtra(Task.TASK_NAME);


                this.tasks.add(new TaskItem(task_name,1));
            }
        }
        else if (requestCode == Task.TASK_EDIT) {
            String task_name = data.getStringExtra(Task.TASK_NAME);
            if (this.position > -1) {
                this.tasks.set(position,new TaskItem(task_name,1));
                adapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    public void onItemClick(View view, int i) {
        Intent intent = new Intent(MainActivity.this, Task.class );
        intent.putExtra(Task.TASK_NAME,this.tasks.get(i).name);
        this.position = i;
        startActivityForResult(intent,Task.TASK_EDIT);
    }
}
