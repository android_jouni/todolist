package fi.oamk.todolist;

public class TaskItem {
    public String name = "";
    public int amount;
    //...

    public TaskItem(String name, int amount) {
        this.name = name;
        this.amount = amount;
    }
}
