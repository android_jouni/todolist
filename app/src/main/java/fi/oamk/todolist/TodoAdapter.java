package fi.oamk.todolist;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class TodoAdapter extends RecyclerView.Adapter<TodoAdapter.ViewHolder> {
    private List<TaskItem> mData;
    private LayoutInflater mInflater;
    private TaskClickListener mTaskClickListener;

    TodoAdapter(Context context,List<TaskItem> data) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
    }


    @NonNull
    @Override
    public TodoAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = mInflater.inflate(R.layout.todo_row,viewGroup,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TodoAdapter.ViewHolder viewHolder, int i) {
        String taskname = mData.get(i).name;
        int amount = mData.get(i).amount;
        viewHolder.txtTaskname.setText(taskname);
        viewHolder.txtAmount.setText(Integer.toString(amount));
    }

    @Override
    public int getItemCount() {
        return mData.size();

    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView txtTaskname;
        TextView txtAmount;

        ViewHolder(View itemView) {
            super(itemView);
            txtTaskname = itemView.findViewById(R.id.txtTaskname);
            txtAmount = itemView.findViewById(R.id.txtAmount);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mTaskClickListener != null) {
                mTaskClickListener.onItemClick(view, getAdapterPosition());
            }
        }
    }

    void setClickListener(TaskClickListener taskClickListener) {
        this.mTaskClickListener = taskClickListener;
    }

    public interface TaskClickListener {
        void onItemClick(View view, int i);
    }
}


